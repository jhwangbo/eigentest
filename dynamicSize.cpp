#include <iostream>
#include "raiCommon/rai_utils.hpp"
#include <Eigen/Core>
#include "eigenFunctions.hpp"

#define dim 40

int main() {
  std::string path = "/home/jhwangbo/Documents";
  rai::Utils::timer->setLogPath(path);

  std::string response;
  response = (char)getchar();
  double r = std::stod(response);

  Eigen::Matrix<double, -1, -1> m1, m2, m3;
  m1.resize(40,40);
  m2.resize(40,40);
  m3.resize(40,40);

  m1.setConstant(r/dim);
  m2.setConstant(r/dim);

  EigenCalss eigenClass;

  for(int i=0; i<1000000; i++) {
    rai::Utils::timer->startTimer("func6");
    eigenClass.func6(m1, m2, m3);
    rai::Utils::timer->stopTimer("func6");
    m1=m3;
  }

  std::cout<<m3<<std::endl;

  for(int i=0; i<1000000; i++) {
    rai::Utils::timer->startTimer("func7");
    eigenClass.func7(m1, m2, m3);
    rai::Utils::timer->stopTimer("func7");
    m1=m3;
  }

  std::cout<<m3<<std::endl;

  for(int i=0; i<1000000; i++) {
    rai::Utils::timer->startTimer("func8");
    eigenClass.func8(m1, m2, m3);
    rai::Utils::timer->stopTimer("func8");
    m1=m3;
  }

  std::cout<<m3<<std::endl;

  for(int i=0; i<1000000; i++) {
    rai::Utils::timer->startTimer("func9");
    eigenClass.func9(m1, m2, m3);
    rai::Utils::timer->stopTimer("func9");
    m1=m3;
  }

  std::cout<<m3<<std::endl;

  for(int i=0; i<1000000; i++) {
    rai::Utils::timer->startTimer("func10");
    eigenClass.func10(m1, m2, m3);
    rai::Utils::timer->stopTimer("func10");
    m1=m3;
  }

  std::cout<<m3<<std::endl;

//  rai::Utils::timer->startTimer("func6");
//  for(int i=0; i<1000000; i++)
//    eigen.func6(m1, m2, m3);
//  rai::Utils::timer->stopTimer("func6");
//
//  rai::Utils::timer->startTimer("func7");
//  for(int i=0; i<1000000; i++)
//    eigen.func7(m1, m2, m3);
//  rai::Utils::timer->stopTimer("func7");

  return 0;
}