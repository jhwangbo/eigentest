#include <iostream>
#include "raiCommon/rai_utils.hpp"
#include <Eigen/Core>
#include "eigenFunctions.hpp"

#define dim 40

int main() {
  std::string path = "/home/jhwangbo/Documents";
  rai::Utils::timer->setLogPath(path);

  std::string response;
  response = (char)getchar();
  double r = std::stod(response);

  Eigen::Matrix<double, -1, 1> m1, m2, m3;
  m1.resize(40,1);

  EigenCalss eigenClass;

//  for(int i=0; i<1000000; i++) {
//    rai::Utils::timer->startTimer("func11");
//    eigenClass.func11(m1.head(12));
//    rai::Utils::timer->stopTimer("func11");
//  }
//
//  for(int i=0; i<1000000; i++) {
//    rai::Utils::timer->startTimer("func12");
//    eigenClass.func12(m1.head(12));
//    rai::Utils::timer->stopTimer("func12");
//  }

  m1.resize(12,1);

  for(int i=0; i<1000000; i++) {
    rai::Utils::timer->startTimer("func13");
    m1 = eigenClass.func13();
    rai::Utils::timer->stopTimer("func13");
  }

  for(int i=0; i<1000000; i++) {
    rai::Utils::timer->startTimer("func14");
    m1 = eigenClass.func14();
    rai::Utils::timer->stopTimer("func14");
  }


  return 0;
}