#include <iostream>
#include "raiCommon/rai_utils.hpp"
#include <Eigen/Core>
#include "eigenFunctions.hpp"

#define dim 40

int main() {
  std::string path = "/home/jhwangbo/Documents";
  rai::Utils::timer->setLogPath(path);

  std::string response;
  response = (char)getchar();
  double r = std::stod(response);

  Eigen::Matrix<double, dim, dim> m1, m2, m3;

  m1.setConstant(r/dim);
  m2.setConstant(r/dim);

  EigenCalss eigenClass;

  for(int i=0; i<1000000; i++) {
    rai::Utils::timer->startTimer("func1");
    eigenClass.func1(m1, m2, m3);
    rai::Utils::timer->stopTimer("func1");
    m1=m3;
  }


  std::cout<<m3<<std::endl;


  for(int i=0; i<1000000; i++) {
    rai::Utils::timer->startTimer("func2");
    eigenClass.func2(m1, m2, m3);
    rai::Utils::timer->stopTimer("func2");
    m1=m3;
  }

  std::cout<<m3<<std::endl;

  for(int i=0; i<1000000; i++) {
    rai::Utils::timer->startTimer("func3");
    eigenClass.func3(m1, m2, m3);
    rai::Utils::timer->stopTimer("func3");
    m1=m3;
  }

  std::cout<<m3<<std::endl;

  for(int i=0; i<1000000; i++) {
    rai::Utils::timer->startTimer("func4");
    eigenClass.func4(m1, m2, m3);
    rai::Utils::timer->stopTimer("func4");
    m1=m3;
  }

  std::cout<<m3<<std::endl;

  for(int i=0; i<1000000; i++) {
    rai::Utils::timer->startTimer("func5");
    eigenClass.func5(m1, m2, m3);
    rai::Utils::timer->stopTimer("func5");
    m1=m3;
  }

  std::cout<<m3<<std::endl;


//  rai::Utils::timer->startTimer("func6");
//  for(int i=0; i<1000000; i++)
//    eigen.func6(m1, m2, m3);
//  rai::Utils::timer->stopTimer("func6");
//
//  rai::Utils::timer->startTimer("func7");
//  for(int i=0; i<1000000; i++)
//    eigen.func7(m1, m2, m3);
//  rai::Utils::timer->stopTimer("func7");

  return 0;
}