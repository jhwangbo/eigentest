//
// Created by jhwangbo on 17. 12. 5.
//

#ifndef EIGENTEST_EIGENFUNCTIONS_HPP
#define EIGENTEST_EIGENFUNCTIONS_HPP

#include <Eigen/Core>
#include <vector>

class EigenCalss {

  /// multiplications
 public:
  EigenCalss() {
    vec.reserve(10000000);
    el.resize(12,12);
  }
  std::vector<Eigen::MatrixXd> vec;
  Eigen::MatrixXd el;


  ///static multi
  void func1(Eigen::Matrix<double, 40, 40> m1, Eigen::Matrix<double, 40, 40> m2, Eigen::Matrix<double, 40, 40> &m3);

  void func2(Eigen::Matrix<double, 40, 40> &m1, Eigen::Matrix<double, 40, 40> &m2, Eigen::Matrix<double, 40, 40> &m3);

  void func3(Eigen::Ref<Eigen::MatrixXd> m1, Eigen::Ref<Eigen::MatrixXd> m2, Eigen::Ref<Eigen::MatrixXd> m3);

  void func4(const Eigen::Ref<Eigen::MatrixXd> m1, const Eigen::Ref<Eigen::MatrixXd> m2, Eigen::Ref<Eigen::MatrixXd> m3);

  void func5(const Eigen::Ref<Eigen::MatrixXd>& m1, const Eigen::Ref<Eigen::MatrixXd>& m2, Eigen::Matrix<double, 40, 40>& m3);


  /// dynamic multi
  void func6(Eigen::MatrixXd m1, Eigen::MatrixXd m2, Eigen::MatrixXd &m3);

  void func7(Eigen::MatrixXd &m1, Eigen::MatrixXd &m2, Eigen::MatrixXd &m3);

  void func8(Eigen::Ref<Eigen::MatrixXd> m1, Eigen::Ref<Eigen::MatrixXd> m2, Eigen::Ref<Eigen::MatrixXd> m3);

  void func9(const Eigen::Ref<Eigen::MatrixXd> m1, const Eigen::Ref<Eigen::MatrixXd> m2, Eigen::Ref<Eigen::MatrixXd> m3);

  void func10(const Eigen::Ref<Eigen::MatrixXd>& m1, const Eigen::Ref<Eigen::MatrixXd>& m2, Eigen::MatrixXd& m3);


  void func11(const Eigen::Ref<Eigen::MatrixXd>& m1);

  void func12(const Eigen::Matrix<double, -1, 1>& m1);


  Eigen::MatrixXd::ColXpr func13();

  Eigen::MatrixXd func14();

//  template<int dim>
//  void func6(const Eigen::Ref<Eigen::Matrix<double, dim, dim>> &m1,
//             const Eigen::Ref<Eigen::Matrix<double, dim, dim>> &m2,
//             Eigen::Ref<Eigen::Matrix<double, dim, dim>> &m3);
//
//  template<int dim>
//  void func7(Eigen::Ref<Eigen::Matrix<double, dim, dim>> m1,
//             Eigen::Ref<Eigen::Matrix<double, dim, dim>> m2,
//             Eigen::Ref<Eigen::Matrix<double, dim, dim>> &m3)

};

#endif //EIGENTEST_EIGENFUNCTIONS_HPP
