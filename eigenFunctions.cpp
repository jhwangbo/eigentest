//
// Created by jhwangbo on 17. 12. 5.
//

#include "eigenFunctions.hpp"

void EigenCalss::func1(Eigen::Matrix<double, 40, 40> m1, Eigen::Matrix<double, 40, 40> m2, Eigen::Matrix<double, 40, 40> &m3){
  m3 = m1 * m2;
}

void EigenCalss::func2(Eigen::Matrix<double, 40, 40> &m1, Eigen::Matrix<double, 40, 40> &m2, Eigen::Matrix<double, 40, 40> &m3) {
  m3 = m1 * m2;
}

void EigenCalss::func3(Eigen::Ref<Eigen::MatrixXd> m1, Eigen::Ref<Eigen::MatrixXd> m2, Eigen::Ref<Eigen::MatrixXd> m3) {
  m3 = m1 * m2;
}

void EigenCalss::func4(const Eigen::Ref<Eigen::MatrixXd> m1, const Eigen::Ref<Eigen::MatrixXd> m2, Eigen::Ref<Eigen::MatrixXd> m3) {
  m3 = m1 * m2;
}

void EigenCalss::func5(const Eigen::Ref<Eigen::MatrixXd>& m1, const Eigen::Ref<Eigen::MatrixXd>& m2, Eigen::Matrix<double, 40, 40>& m3) {
  m3 = m1 * m2;
}


void EigenCalss::func6(Eigen::MatrixXd m1, Eigen::MatrixXd m2, Eigen::MatrixXd &m3){
  m3 = m1 * m2;
}

void EigenCalss::func7(Eigen::MatrixXd &m1, Eigen::MatrixXd &m2, Eigen::MatrixXd &m3){
  m3 = m1 * m2;
}

void EigenCalss::func8(Eigen::Ref<Eigen::MatrixXd> m1, Eigen::Ref<Eigen::MatrixXd> m2, Eigen::Ref<Eigen::MatrixXd> m3){
  m3 = m1 * m2;
}

void EigenCalss::func9(const Eigen::Ref<Eigen::MatrixXd> m1, const Eigen::Ref<Eigen::MatrixXd> m2, Eigen::Ref<Eigen::MatrixXd> m3){
  m3 = m1 * m2;
}

void EigenCalss::func10(const Eigen::Ref<Eigen::MatrixXd>& m1, const Eigen::Ref<Eigen::MatrixXd>& m2, Eigen::MatrixXd& m3){
  m3 = m1 * m2;
}


void EigenCalss::func11(const Eigen::Ref<Eigen::MatrixXd>& m1){
  el = m1;
}

void EigenCalss::func12(const Eigen::Matrix<double, -1, 1>& m1){
  el = m1;
}

Eigen::MatrixXd::ColXpr EigenCalss::func13() {
  return el.col(0);
}

Eigen::MatrixXd EigenCalss::func14() {
  return el.col(0);
}

//template<int dim>
//void EigenCalss::func6(Eigen::Ref<Eigen::Matrix<double, dim, dim>> &m1,
//                       Eigen::Ref<Eigen::Matrix<double, dim, dim>> &m2,
//                       Eigen::Ref<Eigen::Matrix<double, dim, dim>> &m3) {
//  m3 = m1 * m2;
//}
//
//template<int dim>
//void EigenCalss::func7(Eigen::Ref<Eigen::Matrix<double, dim, dim>> m1,
//                       Eigen::Ref<Eigen::Matrix<double, dim, dim>> m2,
//                       Eigen::Ref<Eigen::Matrix<double, dim, dim>> &m3) {
//  m3 = m1 * m2;
//}

